<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
route::resource('jugadores', 'JugadoresController');
route::resource('equipos', 'EquiposController');
route::resource('tipotorneos', 'TiposTorneosController');
route::resource('tipo_usuario', 'TipoUsuarioController');
route::resource('torneos', 'TorneosController');
route::resource('equiposjugadores', 'EquiposJugadoresController');
route::resource('torneosequipos', 'TorneosEquiposController');
route::resource('empresas', 'EmpresasController');
route::resource('sucursales', 'SucursalesController');
route::resource('usuarios', 'UsuariosController');
route::resource('partidos', 'PartidosController');
route::resource('tipoeventos', 'TipoEventosController');
route::resource('eventos', 'EventosController');
route::resource('tipo_estado', 'TipoEstadoController');

route::resource('patrocinadores', 'PatrocinadoresController');
route::resource('puntuacion/torneo', 'PuntuacionTorneoController');
route::resource('puntuacion', 'PuntuacionController');

Route::post('login', 'AuthenticateController@login');
Route::get('logout', 'AuthenticateController@logout');
Route::get('sucursal/{id}/torneos', 'TorneosController@Torneos_Sucursal');
Route::get('equipo/{id}/jugadores', 'EquiposController@Jugadores_Equipos');
Route::get('torneos/{id}/equipos', 'TorneosController@Torneos_Equipos');
Route::get('equipo/{id}/goles', 'PartidosController@Equipos_Goles');
Route::get('partidos/{id}/eventos', 'PartidosController@Partidos_Eventos');
Route::get('partidos/{id}/equipos', 'PartidosController@Partidos_Equipos');
Route::get('partido/{fecha}/equipos', 'PartidosController@Partidos_fecha');
Route::get('eventos/{id}/tipo', 'EventosController@Tipo_Eventos');
route::get('patrocinador/{id}/equipo', 'PatrocinadoresController@Patrocinador_equipos');
Route::get('patrocinador/estado/{estado}', 'PatrocinadoresController@Patrocinadores_Estado');
Route::get('torneo/{id}/puntos', 'TorneosController@Torneo_Puntos');
Route::get('partido/{id}/estado', 'PartidosController@Partidos_Estado');

Route::post('jugadores/foto/{id}', 'JugadoresController@avatar');
