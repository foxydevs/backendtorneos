<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model
{
   protected $table = 'Equipos';

   public function TEjugadores() {
    return $this->hasMany('App\EquiposJugadores', 'equipo', 'id')->select('id','jugador')->with('Jugador');
   
}

}
