<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patrocinadores extends Model
{
    protected $table = 'Patrocinadores';

    public function Equipos() {
        return $this->hasMany('App\PatrocinadoresEquipos', 'patrocinador', 'id')->select('patrocinador','equipo')->with('NEquipos');
       
    }
}
