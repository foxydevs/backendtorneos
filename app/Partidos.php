<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partidos extends Model
{
    protected $table = 'Partidos';
    
    public function Eventos_partido(){
        return $this->hasMany('App\Eventos', 'partido', 'id');
    }
    public function Partido_Estado(){
        return $this->hasMany('App\TipoEstado', 'id', 'estado')->select('estado');
    }

    public function NEquipo1(){
        return $this->hasMany('App\Equipos', 'id', 'equipo_uno')->select('nombre', 'foto');
    }
    public function NEquipo2(){
        return $this->hasMany('App\Equipos', 'id', 'equipo_dos')->select('nombre', 'foto');
    }

    public function Partido_dia(){
        return $this->hasMany('App\Partidos', 'fecha', 'fecha')->select('id', 'fecha', 'hora');
    }
}
