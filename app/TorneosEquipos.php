<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TorneosEquipos extends Model
{
    protected $table = 'TorneosEquipos';

      public function Equipos() {
        return $this->hasOne('App\Equipos', 'id', 'equipo')->select('id', 'nombre');
    }    

    public function Torneos() {
        return $this->hasOne('App\Torneos', 'id', 'torneo')->select('id', 'nombre');
    }    
    
}
