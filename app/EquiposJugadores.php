<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquiposJugadores extends Model
{
    protected $table = 'EquiposJugadores';

    public function Equipos() {
        return $this->hasOne('App\Equipos', 'id', 'equipo')->select('id', 'nombre');
    }    

    public function Jugador() {
        return $this->hasOne('App\Jugadores', 'id', 'jugador')->select('id', 'nombres');
    }  
}
