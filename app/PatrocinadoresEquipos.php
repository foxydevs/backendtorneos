<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatrocinadoresEquipos extends Model
{
    protected $table = 'PatrocinadoresEquipos';

    public function NEquipos() {
        return $this->hasOne('App\Equipos', 'id', 'equipo')->select('id','nombre', 'corto');
       
    }
}
