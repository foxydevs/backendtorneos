<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torneos extends Model
{
   protected $table = 'Torneos';

    public function TEquipos() {
        return $this->hasMany('App\TorneosEquipos', 'torneo', 'id')->select('id','equipo')->with('Equipos');
       
    }

    public function Torneos_Puntos() {
        return $this->hasMany('App\PuntuacionTorneo', 'torneo_equipo', 'id')->select('puntos');
       
    }
    
    public function Tipo_Torneos(){
        return $this->hasOne('App\TiposTorneos', 'id', 'tipotorneo')->select('id', 'nombre');
    }
     
}

