<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Usuarios extends Authenticatable
{
     protected $table = 'Usuarios';
     
     protected $hidden = [
        'password', 'remember_token',
    ];
}
