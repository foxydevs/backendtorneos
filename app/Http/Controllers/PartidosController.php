<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\Partidos;

class PartidosController extends Controller
{
   public function index()
    {
        return Response::json(Partidos::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'equipo_uno' => 'required',
            'equipo_dos' => 'required',
            'fecha' => 'required',
            'hora' => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        } else {
            try {
                $newObject = new Partidos();
                $newObject->equipo_uno = $request->get('equipo_uno');
                $newObject->equipo_dos = $request->get('equipo_dos');
                $newObject->goles_uno = 0;
                $newObject->goles_dos = 0;
                $newObject->fecha = $request->get('fecha');
                $newObject->hora = $request->get('hora');
                $newObject->estado = 1;
                $newObject->save();
                return Response::json($newObject, 200);
            }
            catch(Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $objectSee = Partidos::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Partidos::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->equipo_uno = $request->get('equipo_uno', $objectUpdate->equipo_uno);
                $objectUpdate->equipo_dos = $request->get('equipo_dos', $objectUpdate->equipo_dos);
                $objectUpdate->goles_uno = $request->get('goles_uno', $objectUpdate->goles_uno);
                $objectUpdate->goles_dos = $request->get('goles_dos', $objectUpdate->goles_dos);
                $objectUpdate->fecha = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->hora = $request->get('hora', $objectUpdate->hora);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Partidos::find($id);
        if ($objectDelete) {
            try {
                Partidos::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function Equipos_Goles($id){

            $objectSee = Partidos::whereRaw('equipo_uno = ? || equipo_dos = ?', [$id, $id] )->select('equipo_uno', 'goles_uno', 'equipo_dos', 'goles_dos')->get();
           if ($objectSee) {                
                 
                 foreach($objectSee as $t){
                if($t->equipo_uno == $id){
                    $objectSee = Partidos::whereRaw('equipo_uno = ? || equipo_dos = ?', [$id, $id] )->select('equipo_uno', 'goles_uno')->get();
                    return Response::json($objectSee, 200);        
                 }
                  if($t->equipo_dos == $id){
                    $objectSee = Partidos::whereRaw('equipo_uno = ? || equipo_dos = ?', [$id, $id] )->select('equipo_dos', 'goles_dos')->get();
                    return Response::json($objectSee, 200);        
                 }
                } 
            
            
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }


    public function Partidos_eventos($id){

         $objectSee = Partidos::find($id);

            if ($objectSee) {                
               $objectSee->Eventos_partido;
            
          return Response::json($objectSee, 200);
         }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

     public function Partidos_Equipos($id){

         $objectSee = Partidos::where('id',$id)->select('equipo_uno', 'goles_uno', 'equipo_dos' , 'goles_dos')->first();

            if ($objectSee) {                
                $objectSee->NEquipo1;
                $objectSee->NEquipo2;
          return Response::json($objectSee, 200);
         }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'Not found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function Partidos_Estado($id){
        
                 $objectSee = Partidos::where('id',$id)->select('estado')->first();
        
                if ($objectSee) {    
                    $objectSee->Partido_Estado;            
                    return Response::json($objectSee, 200);
                 }
                else {
                    $returnData = array(
                        'status' => 404,
                        'message' => 'Not found'
                    );
                    return Response::json($returnData, 404);
                }
            }


            public function Partidos_fecha($fecha){
                
                         $objectSee = Partidos::where('fecha',$fecha)->select('fecha')->first();
                
                        if ($objectSee) {    
                            $objectSee->Partido_dia;            
                            return Response::json($objectSee, 200);
                         }
                        else {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'Not found'
                            );
                            return Response::json($returnData, 404);
                        }
                    }

}


