<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Usuarios;
use App\Events;
use App\UserEvents;
use Response;
use Validator;
use Auth;   

class AuthenticateController extends Controller
{

     public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
         else {
             
             try {
                $userdata = array(
                    'username'  => $request->get('username'),
                    'password'  => $request->get('password')
                );              

                if (Auth::attempt($userdata)) {
                    $user = Usuarios::find(Auth::user()->id);

                    return Response::json($user, 200);
                    
                }
                else {
                    $returnData = array (
                        'status' => 401,
                        'message' => 'No valid username or password'
                    );
                    return Response::json($returnData, 401);
                }
             }
             catch (Exception $e) {

                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);


             }
        }
        
    }

    public function logout(Request $request) {
        return 'adios';
        //Auth::logout();
    }
}
