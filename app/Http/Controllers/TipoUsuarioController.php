<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\TipoUsuario;

class TipoUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         return Response::json(TipoUsuario::all(), 200);
     }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
         $validator = Validator::make($request->all(), [
             'tipo' => 'required'
         ]);
 
         if ($validator->fails()) {
             $returnData = array(
                 'status' => 400,
                 'message' => 'Invalid Parameters',
                 'validator' => $validator->messages()->toJson()
             );
             return Response::json($returnData, 400);
         } else {
             try {
                 $newObject = new TipoUsuario();
                 $newObject->tipo = $request->get('tipo');
                 $newObject->estado = 1;
                 $newObject->save();
                 return Response::json($newObject, 200);
             }
             catch(Exception $e) {
                 $returnData = array(
                     'status' => 500,
                     'message' => $e->getMessage()
                 );
                 return Response::json($returnData, 500);
             }
         }
     }
 
     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
        $objectSee = TipoUsuario::find($id);
         if ($objectSee) {
             return Response::json($objectSee, 200);
         }
         else {
             $returnData = array(
                 'status' => 404,
                 'message' => 'Not found'
             );
             return Response::json($returnData, 404);
         }
     }
 
     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
     }
 
     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         $objectUpdate = TipoUsuario::find($id);
         if ($objectUpdate) {
             try {
                 $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                 $objectUpdate->estado = $request->get('estado', $objectUpdate->apellidos);
                 $objectUpdate->save();
                 return Response::json($objectUpdate, 200);
             }
             catch (Exception $e) {
                 $returnData = array(
                     'status' => 500,
                     'message' => $e->getMessage()
                 );
                 return Response::json($returnData, 500);
             }
         }
         else {
             $returnData = array(
                 'status' => 404,
                 'message' => 'Not found'
             );
             return Response::json($returnData, 404);
         }
     }
 
     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         $objectDelete = TipoUsuario::find($id);
         if ($objectDelete) {
             try {
                 TipoUsuario::destroy($id);
                 return Response::json($objectDelete, 200);
             }
             catch (Exception $e) {
                 $returnData = array(
                     'status' => 500,
                     'message' => $e->getMessage()
                 );
                 return Response::json($returnData, 500);
             }
         }
         else {
             $returnData = array(
                 'status' => 404,
                 'message' => 'Not found'
             );
             return Response::json($returnData, 404);
         }
     }
 }
 