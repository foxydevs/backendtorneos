<?php

use Illuminate\Database\Seeder;

class PuntuacionTorneoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('PuntuacionTorneo')->insert(array(
            'torneo_equipo' => 1,
            'puntos'  => 5,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    
        DB::table('PuntuacionTorneo')->insert(array(
            'torneo_equipo' => 1,
            'puntos'  => 4,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

         DB::table('PuntuacionTorneo')->insert(array(
            'torneo_equipo' => 1,
            'puntos'  => 7,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
