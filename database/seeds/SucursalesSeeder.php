<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  public function run()
    {
            DB::table('Sucursales')->insert(array(
            'nombre' => 'sport',
            'telefono'  => '12345678',
            'direccion' => 'Guatemala Zona 2',
            'correo'  => 'sport@sport.com',
            'empresa'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
    ));


            DB::table('Sucursales')->insert(array(
            'nombre' => 'sp',
            'telefono'  => '123443123',
            'direccion' => 'Guatemala Zona 3',
            'correo'  => 'sp@sp.com',
            'empresa'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
    ));

    DB::table('Sucursales')->insert(array(
            'nombre' => 'port',
            'telefono'  => '12345228',
            'direccion' => 'Guatemala Zona 10',
            'correo'  => 'port@port.com',
            'empresa'  => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
    ));
    }
}