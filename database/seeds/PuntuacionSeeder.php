<?php

use Illuminate\Database\Seeder;

class PuntuacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('Puntuacion')->insert(array(
            'puntos' => 1,
            'puntuacion_torneo'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Puntuacion')->insert(array(
            'puntos' => 7,
            'puntuacion_torneo'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Puntuacion')->insert(array(
            'puntos' => 5,
            'puntuacion_torneo'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        
        DB::table('Puntuacion')->insert(array(
            'puntos' => 4,
            'puntuacion_torneo'  => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
