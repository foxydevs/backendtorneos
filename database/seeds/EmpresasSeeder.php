<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
     
           DB::table('Empresas')->insert(array(
            'nombre' => 'Futeca',
            'direccion' => 'zona 14',
            'telefono'  => '12345678',
            'correo'  => 'futeca@futeca.com',
            'estado'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

         DB::table('Empresas')->insert(array(
            'nombre' => 'Profut',
            'direccion' => 'zona 10',
            'telefono'  => '12345678',
            'correo'  => 'profut@profut.com',
            'estado'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}