<?php

use Illuminate\Database\Seeder;

class EquiposJugadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 1,
            'jugador'  => 1,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 1,
            'jugador'  => 2,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 1,
            'jugador'  => 3,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 1,
            'jugador'  => 4,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 2,
            'jugador'  => 5,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 2,
            'jugador'  => 6,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 2,
            'jugador'  => 7,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('EquiposJugadores')->insert(array(
            'equipo' => 2,
            'jugador'  => 8,
            'torneo' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
