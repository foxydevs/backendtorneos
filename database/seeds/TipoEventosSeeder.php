<?php

use Illuminate\Database\Seeder;

class TipoEventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('TipoEventos')->insert(array(
            'nombre' => 'gol',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        )); 

        DB::table('TipoEventos')->insert(array(
            'nombre' => 'Tarjeta Roja',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TipoEventos')->insert(array(
            'nombre' => 'Tarjeta Amarilla',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
