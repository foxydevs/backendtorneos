<?php

use Illuminate\Database\Seeder;


class TorneosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  
        DB::table('Torneos')->insert(array(
            'nombre' => 'Campeonato 1',
            'inicio'  => date('Y-m-d H:m:s'),
            'fin'  =>  date('Y-m-d H:m:s'),
            'sede'  => 1,
            'tipotorneo'  => 1,
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
          DB::table('Torneos')->insert(array(
            'nombre' => 'Campeonato 2',
            'inicio'  => date('Y-m-d H:m:s'),
            'fin'  =>  date('Y-m-d H:m:s'),
            'sede'  => 1,
            'tipotorneo'  => 1,
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        
        DB::table('Torneos')->insert(array(
            'nombre' => 'Campeonato 3',
            'inicio'  => date('Y-m-d H:m:s'),
            'fin'  =>  date('Y-m-d H:m:s'),
            'sede'  => 1,
            'tipotorneo'  => 2,
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Torneos')->insert(array(
            'nombre' => 'Campeonato 4',
            'inicio'  => date('Y-m-d H:m:s'),
            'fin'  =>  date('Y-m-d H:m:s'),
            'sede'  => 2,
            'tipotorneo'  => 2,
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Torneos')->insert(array(
            'nombre' => 'Campeonato 5',
            'inicio'  => date('Y-m-d H:m:s'),
            'fin'  =>  date('Y-m-d H:m:s'),
            'sede'  => 2,
            'tipotorneo'  => 2,
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Torneos')->insert(array(
            'nombre' => 'Campeonato 6',
            'inicio'  => date('Y-m-d H:m:s'),
            'fin'  =>  date('Y-m-d H:m:s'),
            'sede'  => 2,
            'tipotorneo'  => 1,
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}