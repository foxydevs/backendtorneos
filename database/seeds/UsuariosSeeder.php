<?php

use Illuminate\Database\Seeder;


class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        DB::table('Usuarios')->insert(array(
           'nombre' => 'foxy',
           'username'  => 'labs',
           'password' => bcrypt('foxylabs'),
           'correo' => 'foxy@labs.gt',
           'telefono' => '+502 12345678',
           'sede' => 1,
           'avatar' => 'https://cambiardeimagen.files.wordpress.com/2013/03/moda-masculina-lentes-cara-hombre-carametria-caramorfoligia-consultoria-de-imagen.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Usuarios')->insert(array(
           'nombre' => 'Andre',
           'username'  => 'Ajuarez',
           'password' => bcrypt('andrefoxy'),
           'correo' => 'foxyandre@labs.gt',
           'telefono' => '+502 12345678',
           'sede' => 1,
           'avatar' => 'https://cambiardeimagen.files.wordpress.com/2013/03/moda-masculina-lentes-cara-hombre-carametria-caramorfoligia-consultoria-de-imagen.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));


        DB::table('Usuarios')->insert(array(
           'nombre' => 'Dinelfoxy',
           'username'  => 'labsdaniel',
           'password' => bcrypt('foxydaniel'),
           'correo' => 'foxydaniel@labs.gt',
           'telefono' => '+502 12345678',
           'sede' => 2,
           'avatar' => 'https://cambiardeimagen.files.wordpress.com/2013/03/moda-masculina-lentes-cara-hombre-carametria-caramorfoligia-consultoria-de-imagen.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));


        DB::table('Usuarios')->insert(array(
           'nombre' => 'jaimefoxy',
           'username'  => 'jaime',
           'password' => bcrypt('foxyAlejandro'),
           'correo' => 'jaime@labs.gt',
           'telefono' => '+502 12345678',
           'sede' => 2,
           'avatar' => 'https://cambiardeimagen.files.wordpress.com/2013/03/moda-masculina-lentes-cara-hombre-carametria-caramorfoligia-consultoria-de-imagen.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

    }
}
