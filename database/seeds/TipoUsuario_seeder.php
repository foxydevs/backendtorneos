<?php

use Illuminate\Database\Seeder;

class TipoUsuario_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('TipoUsuario')->insert(array(
            'tipo' => 'admin',
            'estado'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TipoUsuario')->insert(array(
            'tipo' => 'user',
            'estado'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TipoUsuario')->insert(array(
            'tipo' => 'jugador',
            'estado'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
