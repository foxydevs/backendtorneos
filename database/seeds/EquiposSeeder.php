<?php

use Illuminate\Database\Seeder;

class EquiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Equipos')->insert(array(
            'nombre' => 'Equipo 1',
            'corto'  => 'E1',
            'foto' => 'http://www.elpoderdelasideas.com/wp-content/uploads/Milwaukee_Admirals_logo-550x493.png',
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Equipos')->insert(array(
            'nombre' => 'Equipo 2',
            'corto'  => 'E2',
            'foto' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkoiX9GoWiYMyeB4yJn0LkGNC1kYG4Pgn5TJHr6TDgYJn1TfZW',
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Equipos')->insert(array(
            'nombre' => 'Equipo 3',
            'corto'  => 'E3',
            'foto' => 'https://i.pinimg.com/736x/7d/85/85/7d858589d45a2f99d3cbb425ac1ede7a--logo-team-bicycle-logo.jpg',
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    
        DB::table('Equipos')->insert(array(
            'nombre' => 'Equipo 4',
            'corto'  => 'E4',
            'foto' => 'http://img.zanda.com/item/15040020000070/1024x768/New_Orleans_Hornets_logo.png',
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

    }
}