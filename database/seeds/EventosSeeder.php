<?php

use Illuminate\Database\Seeder;

class EventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('Eventos')->insert(array(
            'tipoevento' => 1,
            'partido'  => 1,
            'detalle'  => 'Goooooool del jugador 1',
            'minuto'  => date('H:m:s'),
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Eventos')->insert(array(
            'tipoevento' => 1,
            'partido'  => 1,
            'detalle'  => 'Goooooool del jugador 2',
            'minuto'  => date('H:m:s'),
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Eventos')->insert(array(
            'tipoevento' => 1,
            'partido'  => 2,
            'detalle'  => 'Goooooool del jugador 5',
            'minuto'  => date('H:m:s'),
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Eventos')->insert(array(
            'tipoevento' => 1,
            'partido'  => 2,
            'detalle'  => 'Goooooool del jugador 6',
            'minuto'  => date('H:m:s'),
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
