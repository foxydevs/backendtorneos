<?php

use Illuminate\Database\Seeder;

class TipoTorneosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {

        DB::table('TipoTorneos')->insert(array(
            'nombre' => 'eliminatoria',
            'corto'  => 'Eliminacion',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TipoTorneos')->insert(array(
            'nombre' => 'Torneos por fases',
            'corto'  => 'faces',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}