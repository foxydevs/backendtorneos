<?php

use Illuminate\Database\Seeder;

class PatrocinadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('Patrocinadores')->insert(array(
            'nombre' => 'Coca Cola',
            'logo'  => 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Coca-Cola_logo.svg/1200px-Coca-Cola_logo.svg.png',
            'direccion' => 'Finca el Zapote',
            'correo'  => 'jugador5@foxy.gt',
            'descripcion'  => 'coca cola',
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Patrocinadores')->insert(array(
            'nombre' => 'Pepsi Cola',
            'logo'  => 'https://ugc.kn3.net/i/origin/http://www.bligoo.com/media/users/7/394694/images/public/20551/pepsi.jpg?v=1291691010990',
            'direccion' => 'Finca el Zapote',
            'correo'  => 'jugador6@foxy.gt',
            'descripcion'  => 'Pepsi cola',
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
