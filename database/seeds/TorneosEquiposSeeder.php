<?php

use Illuminate\Database\Seeder;

class TorneosEquiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('TorneosEquipos')->insert(array(
            'equipo' => '1',
            'torneo'  => '1',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TorneosEquipos')->insert(array(
            'equipo' => '2',
            'torneo'  => '1',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TorneosEquipos')->insert(array(
            'equipo' => '3',
            'torneo'  => '1',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('TorneosEquipos')->insert(array(
            'equipo' => '4',
            'torneo'  => '1',
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
