<?php

use Illuminate\Database\Seeder;

class PatrocinadoresEquiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('PatrocinadoresEquipos')->insert(array(
            'equipo' => 1,
            'patrocinador'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    

     DB::table('PatrocinadoresEquipos')->insert(array(
            'equipo' => 1,
            'patrocinador'  => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('PatrocinadoresEquipos')->insert(array(
            'equipo' => 2,
            'patrocinador'  => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    
     DB::table('PatrocinadoresEquipos')->insert(array(
            'equipo' => 2,
            'patrocinador'  => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
