<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    // $this->call(UsersTableSeeder::class);
    $this->call(EmpresasSeeder::class);
    $this->call(SucursalesSeeder::class);
    $this->call(UsuariosSeeder::class);
    $this->call(TipoTorneosSeeder::class);
    $this->call(TorneosSeeder::class);
    $this->call(EquiposSeeder::class);
    $this->call(JugadoresSeeder::class);
    $this->call(EquiposJugadoresSeeder::class);
    $this->call(TorneosEquiposSeeder::class);
    $this->call(TipoEstadoSeeder::class);
    $this->call(PartidosSeeder::class);
    $this->call(TipoEventosSeeder::class);
    $this->call(EventosSeeder::class);
    $this->call(PatrocinadoresSeeder::class);
    $this->call(PatrocinadoresEquiposSeeder::class);
    $this->call(PuntuacionTorneoSeeder::class);
    $this->call(PuntuacionSeeder::class);
    $this->call(TipoUsuario_seeder::class);
     }
}
