<?php

use Illuminate\Database\Seeder;

class PartidosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Partidos')->insert(array(
            'equipo_uno' => 1,
            'equipo_dos'  => 2,
            'goles_uno' => 0,
            'goles_dos'  => 2,
            'fecha'  => date('Y-m-d'),
            'hora'  => date('H:i:s'),
            'estado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Partidos')->insert(array(
            'equipo_uno' => 3,
            'equipo_dos'  => 4,
            'goles_uno' => 0,
            'goles_dos'  => 2,
            'fecha'  => date('Y-m-d'),
            'hora'  => date('H:i:s'),
            'estado' => 2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }
}
