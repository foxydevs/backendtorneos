<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class JugadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 1',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador1@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'http://www.recreoviral.com/wp-content/uploads/2016/04/Fot%C3%B3grafo-Marcos-Alberti-muestra-que-eres-m%C3%A1s-feliz-despu%C3%A9s-de-la-3ra-copa-de-vino-1.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 2',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador2@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'http://memeguy.com/photos/images/pic-2-artist-combines-celebrity-faces-206612.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 3',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador3@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'https://i.pinimg.com/736x/1d/f1/cb/1df1cbe0d0b62fa58112cdc607d793b3--hottest-actors-male-faces.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 4',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador4@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'http://images2.fanpop.com/images/photos/3000000/Jensen-Ackles-jensen-ackles-3028194-333-500.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 5',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador5@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTUyMzQ4MzM5MV5BMl5BanBnXkFtZTgwNDIzNTA5NzE@._V1_UY1200_CR161,0,630,1200_AL_.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 6',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador6@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'http://2.bp.blogspot.com/--0UTYYDlUqE/Vl4i5V-h60I/AAAAAAAANow/3AnRkSnF2_c/s1600/4.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 7',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador7@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'http://www2.pictures.zimbio.com/gi/Dave+Franco+ELLE+17th+Annual+Women+Hollywood+7-o3PorsMHwl.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

        DB::table('Jugadores')->insert(array(
           'nombres' => 'jugador 8',
           'apellidos'  => 'Apellido Jugador',
           'edad' => 21,
           'telefono' => '12345678',
           'correo' => 'jugador8@foxy.gt',
           'direccion' => 'Finca el Zapote',
           'foto' => 'http://images2.fanpop.com/images/photos/6700000/jacob-black-twilight-guys-6725695-391-500.jpg',
           'estado' => 1,
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
        ));

    }
}
