<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipoevento')->unsigned();
            $table->integer('partido')->unsigned();
            $table->string('detalle');
            $table->string('minuto');
            $table->timestamps();
            $table->foreign('tipoevento')->references('id')->on('TipoEventos');
            $table->foreign('partido')->references('id')->on('Partidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Eventos');
    }
}
