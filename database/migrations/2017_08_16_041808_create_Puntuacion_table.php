<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntuacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Puntuacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('puntos');
            $table->integer('puntuacion_torneo')->unsigned();
            $table->timestamps();
            $table->foreign('puntuacion_torneo')->references('id')->on('PuntuacionTorneo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Puntuacion');
    }
}
