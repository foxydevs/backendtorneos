<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntuacionTorneoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PuntuacionTorneo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('torneo_equipo')->unsigned();
            $table->integer('puntos');
            $table->timestamps();
            $table->foreign('torneo_equipo')->references('id')->on('TorneosEquipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PuntuacionTorneo');
    }
}
