<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJugadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Jugadores', function (Blueprint $table) {
            $table->increments('id');
             $table->string("nombres");
            $table->string("apellidos");
            $table->integer("edad");
            $table->string("telefono");
            $table->string("correo")->unique();
            $table->string("direccion");
            $table->string("foto");
            $table->boolean('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Jugadores');
    }
}
