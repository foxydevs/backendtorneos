<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Partidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipo_uno')->unsigned();
            $table->integer('equipo_dos')->unsigned();
            $table->integer('goles_uno');
            $table->integer('goles_dos');
            $table->date('fecha');
            $table->time('hora');
            $table->integer('estado')->unsigned();
            $table->timestamps();
            $table->foreign('equipo_uno')->references('id')->on('TorneosEquipos');
            $table->foreign('equipo_dos')->references('id')->on('TorneosEquipos');
            $table->foreign('estado')->references('id')->on('TipoEstado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Partidos');
    }
}
