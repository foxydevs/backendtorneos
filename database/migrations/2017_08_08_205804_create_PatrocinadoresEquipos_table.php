<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrocinadoresEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PatrocinadoresEquipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipo')->unsigned();
            $table->integer('patrocinador')->unsigned();
            $table->timestamps();
            $table->foreign('equipo')->references('id')->on('Equipos');
            $table->foreign('patrocinador')->references('id')->on('Patrocinadores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PatrocinadoresEquipos');
    }
}
