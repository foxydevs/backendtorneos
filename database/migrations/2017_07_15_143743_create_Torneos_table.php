<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTorneosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Torneos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->datetime('inicio');
            $table->datetime('fin');
            $table->integer('sede')->unsigned();
            $table->integer('tipotorneo')->unsigned();
            $table->boolean('estado');
            $table->timestamps();
            $table->foreign('sede')->references('id')->on('Sucursales');
            $table->foreign('tipotorneo')->references('id')->on('TipoTorneos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Torneos');
    }
}
