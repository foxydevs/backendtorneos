<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquiposJugadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EquiposJugadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipo')->unsigned();
            $table->integer('jugador')->unsigned();
            $table->integer('torneo')->unsigned();
            $table->timestamps();
            $table->foreign('equipo')->references('id')->on('Equipos');
            $table->foreign('jugador')->references('id')->on('Jugadores');
            $table->foreign('torneo')->references('id')->on('Torneos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EquiposJugadores');
    }
}
