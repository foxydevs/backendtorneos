<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('Sucursales', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nombre")->unique();
            $table->string("telefono");
            $table->string("direccion");
            $table->string("correo")->unique();
            $table->integer("empresa")->unsigned();
            $table->timestamps();
            $table->foreign('empresa')->references('id')->on('Empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('Sucursales');
    }
}
