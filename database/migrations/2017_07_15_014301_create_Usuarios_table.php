<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nombre");
            $table->string("username")->unique();
            $table->string("password");
            $table->string("correo")->unique();
            $table->string("telefono");            
            $table->integer("sede")->unsigned();
            $table->string("avatar");
            $table->boolean("estado");
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('sede')->references('id')->on('Sucursales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Usuarios');
    }
}
